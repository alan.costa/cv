# Hello, welcome!

**Here you can find my resume.**

I made this gitlab project to automate the update of my resume.

You can see my resume in English and Portuguese on these two files "Alan Fernandes da Costa - Resume.md" and "Alan Fernandes da Costa - CV.md"

# Olá, Bem-vinda(o)!

**Aqui você encontra meu currículo.**

Eu criei esse projeto no gitlab para automatizar a atualização do meu currículo.

Você pode consultá-lo em inglês e português através dos arquivos "Alan Fernandes da Costa - Resume.md" e "Alan Fernandes da Costa - CV.md"

---------------------------------------------------------------------------------------

You can use pandoc to convert the .md file to .pdf

$pandoc Alan\ Fernandes\ da\ Costa\ -\ Resume.md -o Alan\ Fernandes\ da\ Costa\ -\ Resume.pdf
